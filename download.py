import colorama
import requests
import zipfile
from io import BytesIO
from tqdm import tqdm

def download_from_zip_link(
    username: str,
    name: str,
    branch: str,
    file_path: str,
    output: str,
    type_download: str = "archive",
):
    link=f"https://gitlab.com/{username}/{name}/-/{type_download}/{branch}/{file_path}"
    try:
        r = requests.get(link, stream=True)
    except Exception as e:
        print("Error:", e)
        return
    if r.status_code != 200:
        print("Error:", r.status_code)
        return
    if type_download == "archive":

        try:
            z = zipfile.ZipFile(BytesIO(r.content))
        except zipfile.BadZipFile:
            raise Exception("Bad Zip File")
        except zipfile.LargeZipFile:
            raise Exception("Large Zip File")
        def progress_bar(members):
            try:
                for member in tqdm(members, desc='Extracting '):
                    print(member)
                    yield member
            except Exception as e:
                print(e)
                
        try:
            z.extractall(path=output, members=progress_bar(z.infolist()))        
        except Exception as e:
            print(e)
        z.close()
    else:
        # write file and verbose progress bar
        total_length = int(r.headers.get("content-length"))
        with open(file_path, "wb") as f:
            for chunk in tqdm(
                r.iter_content(chunk_size=1024),
                total=total_length / 1024,
                unit="KB",
                desc="Downloading",
            ):
                if chunk:
                    f.write(chunk)
                    f.flush()

if __name__ == '__main__':
    # demo download file from gitlab
    try:
        download_from_zip_link(
            username="ltn281097",
            name="test-download-app",
            type_download="raw",
            branch="master",
            file_path="Zoom.pkg",
            output="downloads",
        )
        print(colorama.Fore.GREEN + "Downloaded with demo file single" + colorama.Fore.RESET)
    except Exception as e:
        print(colorama.Fore.RED + "Error: " + str(e) + colorama.Style.RESET_ALL)
    
    # demo download zip file from gitlab
    try:
        download_from_zip_link(
            username="ltn281097",
            name="test-download-app",
            type_download="archive",
            branch="master",
            file_path="test-download-app-master.zip",
            output="downloads",
        )
        print(colorama.Fore.GREEN + "Downloaded with demo zip file" + colorama.Fore.RESET)
    except Exception as e:
        print(colorama.Fore.RED + "Error: " + str(e) + colorama.Style.RESET_ALL)